function call_side(caller, params, event) {
	
	var side_popup_obj = {};
	side_popup_obj.popup_params = {};
	if( params !== undefined && params !== null && typeof params === 'object' ) {
		side_popup_obj.popup_params = params;
	}
	
	var $caller = $(caller);
	var clone_events;
	if( $caller.length > 0 ) {
		var target = $caller.data("target");
		if( target !== "" && target !== undefined && target !== null ) {
			side_popup_obj.popup_params.target = target;
		}
		var side = $caller.data("side");
		if( side !== "" && side !== undefined && side !== null ) {
			side_popup_obj.popup_params.side = side;
		}
		var width = $caller.data("side-width");
		if(width !== undefined && width !== null && !isNaN(parseFloat(width))) {
			side_popup_obj.popup_params.width = width;
		}
		
		clone_events = $caller.data("clone-events");
		if(clone_events !== undefined && clone_events !== null && clone_events !== "") {
			side_popup_obj.popup_params.clone_events = clone_events;
		}
	}
	
	clone_events = false;
	if(side_popup_obj.popup_params.clone_events === true) {
		clone_events = true;
	}
	
	side_popup_obj.content = null;
	var $target = $(side_popup_obj.popup_params.target);
	if( $target.length <= 0 ) {
		side_popup_obj.content = "<div class='popup-error'>К сожалению, произошла ошибка.</div>";
		init_side(side_popup_obj.content);
		side_popup_obj.error = "целевой блок не найден.";
		console.warn(side_popup_obj.error);
	}
	
	side_popup_obj.content = $target.clone(clone_events);
	if( side_popup_obj.content !== undefined && side_popup_obj.content !== null && side_popup_obj.content !== "" ) {
		init_side(side_popup_obj.content, side_popup_obj.popup_params);
	}
	
	return side_popup_obj;
}

function init_side(content, params) {
	
	var side_popup_obj = {};
	if(
		content === null
		||
		content === undefined
		||
		content === ""
	) {
		content = "<div class='popup-error'>К сожалению, произошла ошибка.</div>";
		side_popup_obj.error = "пустой контент.";
		console.warn(side_popup_obj.error);
	}
	
	side_popup_obj.content = $(content);
	if( side_popup_obj.content.length === 0 ) {
		side_popup_obj.error = "Неудалось преобразовать контент в jQuery object";
		console.warn(side_popup_obj.error);
		return side_popup_obj;
	}
	
	side_popup_obj.popup_params = {};
	if(params !== undefined && params !== null && typeof params === "object") {
		side_popup_obj.popup_params = params;
	}
	
	if(side_popup_obj.popup_params.side !== "left" && side_popup_obj.popup_params.side !== "right") {
		side_popup_obj.popup_params.side = "left";
	}
	
	side_popup_obj.popup_params.slided_class = "";
	switch( side_popup_obj.popup_params.side ) {
		case "right":
			side_popup_obj.popup_params.slided_class = "shown-right-side-popup";
			break;
		case "left":
			side_popup_obj.popup_params.slided_class = "shown-left-side-popup";
			break;
		default:
			side_popup_obj.popup_params.slided_class = "shown-left-side-popup";
			break;
	}
	
	side_popup_obj.side_popup_overlay = document.createElement("div");
	side_popup_obj.side_popup_overlay.className = "side-popup-overlay";
	
	side_popup_obj.side_popup = document.createElement("div");
	side_popup_obj.side_popup.className = "side-popup " + side_popup_obj.popup_params.side;
	
	if(!isNaN(parseFloat(side_popup_obj.popup_params.width))) {
		side_popup_obj.side_popup.style.width = parseFloat(side_popup_obj.popup_params.width) + "px";
	}
	
	side_popup_obj.side_popup_body = document.createElement("div");
	side_popup_obj.side_popup_body.className = "side-popup-body";
	side_popup_obj.side_popup.appendChild(side_popup_obj.side_popup_body);
	
	var body = document.body;
	body.appendChild(side_popup_obj.side_popup_overlay);
	body.appendChild(side_popup_obj.side_popup);
	
	if( !side_popup_obj.content.is(":visible") ) {
		
		side_popup_obj.content.css({ display: "block" });
	}
	$(side_popup_obj.side_popup_body).html(side_popup_obj.content);
	
	$(side_popup_obj.side_popup_overlay).click(function() {
		remove_side(side_popup_obj);
	});
	
	if(typeof side_popup_obj.popup_params.onInit === "function") {
		side_popup_obj.popup_params.onInit(side_popup_obj);
	}
	
	show_side(side_popup_obj);
	
	return side_popup_obj;
}

function show_side(side_popup_obj) {
	
	if(typeof side_popup_obj.popup_params.beforeShow === "function") {
		side_popup_obj.popup_params.beforeShow(side_popup_obj);
	}
	
	$("html,body").addClass("ovh");
	$(side_popup_obj.side_popup_overlay).show();
	$(side_popup_obj.side_popup).show();
	$(side_popup_obj.popup_params.slided_elements).each(function(index, item) {
		var $slided_element = $(item);
		if( $slided_element.length > 0 ) {
			$slided_element.addClass("shown-side-popup " + side_popup_obj.popup_params.slided_class);
		}
	});
	
	if(typeof side_popup_obj.popup_params.afterShow === "function") {
		side_popup_obj.popup_params.afterShow(side_popup_obj);
	}
	
	return side_popup_obj;
}

function hide_side(side_popup_obj) {
	
	if(typeof side_popup_obj.popup_params.beforeHide === "function") {
		side_popup_obj.popup_params.beforeHide(side_popup_obj);
	}
	
	if( side_popup_obj.side_popup === undefined || side_popup_obj.side_popup === null ) {
		side_popup_obj.side_popup = $(".popup");
	}
	if( side_popup_obj.side_popup_overlay === undefined || side_popup_obj.side_popup_overlay === null ) {
		side_popup_obj.side_popup_overlay = $(".popup-overlay");
	}
	$(side_popup_obj.side_popup).hide();
	$(side_popup_obj.side_popup_overlay).hide();
	$(side_popup_obj.popup_params.slided_elements).each(function(index, item) {
		var $slided_element = $(item);
		if( $slided_element.length > 0 ) {
			$slided_element.removeClass("shown-side-popup " + side_popup_obj.popup_params.slided_class);
		}
	});
	$("html,body").removeClass("ovh");
	
	if(typeof side_popup_obj.popup_params.afterHide === "function") {
		side_popup_obj.popup_params.afterHide(side_popup_obj);
	}
	
	return side_popup_obj;
}

function remove_side(side_popup_obj) {
	
	if(typeof side_popup_obj.popup_params.beforeRemove === "function") {
		side_popup_obj.popup_params.beforeRemove(side_popup_obj);
	}
	
	if( side_popup_obj.side_popup === undefined || side_popup_obj.side_popup === null ) {
		side_popup_obj.side_popup = $(".side-popup");
		if( side_popup_obj.side_popup.length <= 0 ) {
			return;
		}
	}
	
	if( side_popup_obj.side_popup_overlay === undefined || side_popup_obj.side_popup_overlay === null ) {
		side_popup_obj.side_popup_overlay = $(".side-popup-overlay");
		if( side_popup_obj.side_popup_overlay.length <= 0 ) {
			return;
		}
	}
	
	$(side_popup_obj.side_popup).remove();
	$(side_popup_obj.side_popup_overlay).remove();
	
	$(side_popup_obj.popup_params.slided_elements).each(function(index, item) {
		var $slided_element = $(item);
		if( $slided_element.length > 0 ) {
			$slided_element.removeClass("shown-side-popup " + side_popup_obj.popup_params.slided_class);
		}
	});
	
	$("html,body").removeClass("ovh");
	
	if(typeof side_popup_obj.popup_params.afterRemove === "function") {
		side_popup_obj.popup_params.afterRemove(side_popup_obj);
	}
	
	return side_popup_obj;
}